<?php

require_once("xmlrpc.inc");

global $PBAPIServer;
$PBAPIServer = "www.phoneblogz.com";

// Retrieve the users for the given account code and password
function getUsersForAccount($accesscode, $password) {

  global $PBAPIServer;

  $f = new xmlrpcmsg("pb.getUsersForAccount",
                array(new xmlrpcval($accesscode, "string"),
                      new xmlrpcval($password, "string")
                     )
                    );

  $c = new xmlrpc_client("/pbapi/xmlrpc.php", $PBAPIServer, 80);

  $r=$c->send($f, 1);

  $arrResults = array();

  if ($r->faultCode() != 0) {
    $arrResults = array("error" => $r->faultString());
  } else {
    $v=$r->value();
    $u=$v->scalarval();

    // Go through the structs
    for ($i = 0; $i < count($u); ++$i) {
      $arr = $u[$i]->scalarval();
      array_push($arrResults, array("id" => $arr["id"]->scalarval(),
                                    "pin" => $arr["pin"]->scalarval(),
                                    "name" => $arr["name"]->scalarval()));
    }
  }

  return $arrResults;
}

// Delete a user from the given account
function deleteUserFromAccount($accesscode, $password, $userno) {
  
}

// Retrieve the users for the given account code and password
function addUserToAccount($accesscode, $password, $newname, $newemail) {

  global $PBAPIServer;

  $f = new xmlrpcmsg("pb.addUserToAccount",
                array(new xmlrpcval($accesscode, "string"),
                      new xmlrpcval($password, "string"),
                      new xmlrpcval($newname, "string"),
                      new xmlrpcval($newemail, "string")
                     )
                    );

  $c = new xmlrpc_client("/pbapi/xmlrpc.php", $PBAPIServer, 80);

  $r=$c->send($f, 1);

  $arrResults = array();

  if ($r->faultCode() != 0) {
    $arrResults = array("error" => $r->faultString());
  } else {
    $v=$r->value();
    $u=$v->scalarval();
    $arrResults = array("pin" => $u["pin"]->scalarval(), "userid" => $u["userid"]->scalarval());
  }

  return $arrResults;
}

function updateWordpressOptions($accesscode, $password, $notifyurl) {

  global $PBAPIServer;

  $f = new xmlrpcmsg("pb.updateWordpressOptions",
                array(new xmlrpcval($accesscode, "string"),
                      new xmlrpcval($password, "string"),
                      new xmlrpcval($notifyurl, "string")
                     )
                    );
  $c = new xmlrpc_client("/pbapi/xmlrpc.php", $PBAPIServer, 80);
  $r=$c->send($f, 1);

  if ($r->faultCode() != 0) {
    $arrResults = array("error" => $r->faultString());
  } else {
    $v=$r->value();
    $u=$v->scalarval();
    $arrResults = array("data" => $u);
  }

  return $arrResults;
}

function updateWordpressOptionsForUser($accesscode, $password, $pincode, $notifyurl) {

  global $PBAPIServer;

  $f = new xmlrpcmsg("pb.updateWordpressOptionsForUser",
                array(new xmlrpcval($accesscode, "string"),
                      new xmlrpcval($password, "string"),
                      new xmlrpcval($pincode, "string"),
                      new xmlrpcval($notifyurl, "string")
                     )
                    );

  $c = new xmlrpc_client("/pbapi/xmlrpc.php", $PBAPIServer, 80);
  $r=$c->send($f, 1);

  if ($r->faultCode() != 0) {
    $arrResults = array("error" => $r->faultString());
  } else {
    $v=$r->value();
    $u=$v->scalarval();
    $arrResults = array("data" => $u[status]->scalarval(), 
                        "userno" => $u[userno]->scalarval(),
                        "username" => $u[username]->scalarval());
  }

  return $arrResults;
}


function deleteMessage($accesscode, $password, $msgid) {

  global $PBAPIServer;

  $f = new xmlrpcmsg("pb.deleteMessage",
                array(new xmlrpcval($accesscode, "string"),
                      new xmlrpcval($password, "string"),
                      new xmlrpcval($msgid, "string")
                     )
                    );

  $c = new xmlrpc_client("/pbapi/xmlrpc.php", $PBAPIServer, 80);
  $r=$c->send($f, 1);

  if ($r->faultCode() != 0) {
    $arrResults = array("error" => $r->faultString());
  } else {
    $v=$r->value();
    $u=$v->scalarval();
    $arrResults = array("data" => $u);
  }

  return $arrResults;
}


function getPostsForAccount($accesscode, $password) {
  global $PBAPIServer;

  $f = new xmlrpcmsg("pb.getPostsForAccount",
                array(new xmlrpcval($accesscode, "string"),
                      new xmlrpcval($password, "string")
                     )
                    );

  $c = new xmlrpc_client("/pbapi/xmlrpc.php", $PBAPIServer, 80);

  $r=$c->send($f, 1);

  $arrResults = array();

  if ($r->faultCode() != 0) {
    $arrResults = array("error" => $r->faultString());
  } else {
    $v=$r->value();
    $u=$v->scalarval();

    // Go through the structs
    for ($i = 0; $i < count($u); ++$i) {
      $arr = $u[$i]->scalarval();
      array_push($arrResults, array("messageid" => $arr["messageid"]->scalarval(),
                                    "userno" => $arr["userno"]->scalarval(),
                                    "username" => $arr["username"]->scalarval(),
                                    "timeleft" => $arr["timeleft"]->scalarval(),
                                    "callerid" => $arr["callerid"]->scalarval(),
                                    "processed" => $arr["processed"]->scalarval(),
                                    "onhold" => $arr["onhold"]->scalarval(),
                                    "pin" => $arr["pin"]->scalarval()));
    }
  }

  return $arrResults;
}

function getPostsForAccountUser($accesscode, $password, $userno) {
  global $PBAPIServer;

  $f = new xmlrpcmsg("pb.getPostsForAccountUser",
                array(new xmlrpcval($accesscode, "string"),
                      new xmlrpcval($password, "string"),
                      new xmlrpcval($userno, "string")
                     )
                    );

  $c = new xmlrpc_client("/pbapi/xmlrpc.php", $PBAPIServer, 80);
  $r=$c->send($f, 1);

  $arrResults = array();

  if ($r->faultCode() != 0) {
    $arrResults = array("error" => $r->faultString());
  } else {
    $v=$r->value();
    $u=$v->scalarval();

    // Go through the structs
    for ($i = 0; $i < count($u); ++$i) {
      $arr = $u[$i]->scalarval();
      array_push($arrResults, array("messageid" => $arr["messageid"]->scalarval(),
                                    "userno" => $arr["userno"]->scalarval(),
                                    "username" => $arr["username"]->scalarval(),
                                    "timeleft" => $arr["timeleft"]->scalarval(),
                                    "callerid" => $arr["callerid"]->scalarval(),
                                    "processed" => $arr["processed"]->scalarval(),
                                    "onhold" => $arr["onhold"]->scalarval()));
    }
  }

  return $arrResults;
}



function getAudioFile($accesscode, $password, $messageid) {

  global $PBAPIServer;

  $f = new xmlrpcmsg("pb.getAudioFile",
                array(new xmlrpcval($accesscode, "string"),
                      new xmlrpcval($password, "string"),
                      new xmlrpcval($messageid, "int")
                     )
                    );
  $c = new xmlrpc_client("/pbapi/xmlrpc.php", $PBAPIServer, 80);
  $r=$c->send($f, 1);

  $arrResults = array();

  if ($r->faultCode() != 0) {
    $arrResults = array("error" => $r->faultString());
  } else {
    $v=$r->value();
    $u=$v->scalarval();
    $arrResults = array("data" => $u);
  }

  return $arrResults;
}

?>
